var exports = module.exports = {};

function numberFactory(number) {
  return function(fn) {
    return fn === undefined ? number : fn(number);
  }
}

exports.numberFactory = numberFactory;

exports.times = (number) => (otherNumber) => (otherNumber * number);
exports.dividedBy = (number) => (otherNumber) => (otherNumber / number);
exports.plus = (number) => (otherNumber) => (otherNumber + number);
exports.minus = (number) => (otherNumber) => (otherNumber - number);

exports.zero = numberFactory(0);
exports.one = numberFactory(1);
exports.two = numberFactory(2);
exports.three = numberFactory(3);
exports.four = numberFactory(4);
exports.five = numberFactory(5);
exports.six = numberFactory(6);
exports.seven = numberFactory(7);
exports.eight = numberFactory(8);
exports.nine = numberFactory(9);
