import test from 'ava';

import {
  numberFactory,
  zero,
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  nine,
  plus,
  minus,
  times,
  dividedBy
} from './src/calculator.js';

test('number factory function returns number when no function provided', t => {
  const one = numberFactory(1);
  t.is(one(), 1);
})

test('number factory returns a function of the number when function provided', t => {
  const nine = numberFactory(9);
  const squared = (number) => (number * number);

  t.is(nine(squared), 81);
})

test('7 * 5', t => {
  t.is(seven(times(five())), 35);
});

test('4 + 9', t => {
  t.is(four(plus(nine())), 13);
});

test('8 - 3', t=> {
  t.is(eight(minus(three())), 5);
});

test('6 / 2', t => {
  t.is(six(dividedBy(two())), 3);
});

test('5 * 0', t => {
  t.is(five(times(zero())), 0);
})

test('1 + 1 + 1', t => {
  t.is(one(plus(one(plus(one())))), 3);
})
