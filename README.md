# Calculator #

This is simple calculator test using javascript function closures.

## Installation ##

npm install && npm start

## Structure ##

The main file is `src/calculator.js` but everything is currently run through `test.js`. For instance, running `npm start` and `npm test` simply call `ava test.js`.
